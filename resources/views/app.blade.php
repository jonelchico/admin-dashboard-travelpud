<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Travelpud </title>

    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">

</head>

<body>
    <div id="root"></div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>