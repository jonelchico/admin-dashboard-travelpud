export const ROOT = '/';
export const SIGNIN = '/sign-in';
export const SIGNUP = '/sign-up';
export const OVERVIEW = '/overview';
export const ID_SUBMISSIONS = '/id-submissions';
export const USER_FEEDBACK = '/user-feedback';