import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

const UserFeedback = (props) => {
	return <SvgIcon viewBox="0 0 18 18" {...props}>
		<path class="a" d="M10,13H5l1.828-2H13a1,1,0,0,0,1-1V4h1a1,1,0,0,1,1,1v7a1,1,0,0,1-1,1H13v3Z" /><path class="a" d="M3,9H1A1,1,0,0,1,0,8V1A1,1,0,0,1,1,0H11a1,1,0,0,1,1,1V8a1,1,0,0,1-1,1H6L3,12Z" />
	</SvgIcon>
};

export { UserFeedback };