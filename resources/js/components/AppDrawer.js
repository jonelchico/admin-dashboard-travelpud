import React, { useState, useEffect } from 'react';
import { Drawer, Hidden, Grid, Typography, Avatar, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import { drawerWidth } from './context/LayoutContext'
import logoIcon from './assets/images/groupLogo.png';
import TravelpudLogoName from './assets/icons/travelpud-logo-name';
import {
    ROOT,
    SIGNIN,
    SIGNUP,
    OVERVIEW,
    ID_SUBMISSIONS,
    USER_FEEDBACK
} from './paths';
import { DashboardIcon, IdSubmissionsIcon, UserFeedback } from './assets/icons'

const useStyles = makeStyles(theme => ({
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    drawerPaper: {
        width: drawerWidth,
        zIndex: 1,
        backgroundColor: theme.palette.primary.main
    },
    logo: {
        margin: 0,
        width: theme.spacing(4),
        height: theme.spacing(4),
    },
    logoName: {
        width: theme.spacing(12),
        fill: theme.palette.white.main
    },
    logoContainer: {
        padding: theme.spacing(1)
    },
    logoContainerHover: {
        padding: theme.spacing(1),
        cursor: 'pointer'
    },
    listContainer: {
        width: '100%',
    },
    listItemContainer: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    listItemIcon: {
        maxWidth: 20,
        paddingLeft: theme.spacing(2)
    },
    listItemText: {
        fontSize: '16px',
        color: theme.palette.white.main
    },
    icon: {
        fill: theme.palette.white.darker,
        width: 18,
        height: 18
    },
    iconSelected: {
        fill: theme.palette.white.main,
        width: 18,
        height: 18
    },
    menuItemSelected: {
        backgroundColor: 'rgba(0, 0, 0, 0.1)',
        borderLeft: '5px solid #FFF',
    },
    iconContainer: {
        marginLeft: '-5px'
    }


}));

export default withRouter(function ResponsiveDrawer(props) {
    const { container, history, location } = props;
    const classes = useStyles();
    const theme = useTheme();

    const mainMenuPath = `/${location.pathname.split('/')[1]}`;
    const [selectedMainMenuItem, setSelectedMainMenuItem] = useState(`${mainMenuPath}`);

    function pushHistory(url) {
        history.push(url)
    }

    const MyListItem = ({ text, action, listItemIcon, selected, noti, classes }) => {
        let className = selected ? classes.menuItemSelected : '';
        let iconContainerCondition = selected ? classes.iconContainer : null
        return <Grid container className={classes.listItemContainer} >
            <ListItem
                className={className}
                button
                divider={false}
                onClick={action}
            >
                <Grid container direction="row" alignItems="center" justify="space-between" >
                    <Grid item xs={9} >
                        <Grid className={iconContainerCondition} container direction="row" alignItems="center" justify="space-evenly" >
                            <Grid item xs={1} container justify="center">
                                {listItemIcon}
                            </Grid>
                            <Grid item xs={8}>
                                <ListItemText  >
                                    <Typography className={classes.listItemText} variant="body2">{text}</Typography>
                                </ListItemText>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={2}>
                        {
                            noti ?
                                <ListItemText>
                                    <Typography className={classes.listItemText} align="right">0</Typography>
                                </ListItemText>
                                : null
                        }
                    </Grid>
                </Grid>
            </ListItem>
        </Grid>
    }

    const drawer = (
        <>
            <Grid container direction="column" alignItems="center" style={{ minHeight: '100vh', }}>
                <Grid item container direction="column" alignItems="center" className={classes.logoContainer}>
                    <Grid item className={classes.logoContainerHover} onClick={() => pushHistory(ROOT)} >
                        <Avatar alt="Travelpud" src={logoIcon} className={classes.logo}  />
                    </Grid>
                    <Grid item className={classes.logoContainerHover} onClick={() => pushHistory(ROOT)}>
                        <TravelpudLogoName className={classes.logoName}  />
                        {/* <svg  src={logoName} className={classes.logoName} /> */}
                    </Grid>
                </Grid>
                <Grid item constainer className={classes.listContainer} >
                    <List disablePadding={true} >
                        <MyListItem
                            selected={selectedMainMenuItem === OVERVIEW}
                            classes={classes}
                            text="Overview"
                            listItemIcon={
                                <ListItemIcon className={classes.listItemIcon}>
                                    {
                                        selectedMainMenuItem === OVERVIEW ?
                                            <DashboardIcon className={classes.iconSelected} /> :
                                            <DashboardIcon className={classes.icon} />
                                    }
                                </ListItemIcon>
                            }
                            action={() => pushHistory(OVERVIEW)} />
                        <MyListItem
                            selected={selectedMainMenuItem === ID_SUBMISSIONS}
                            classes={classes}
                            text="ID Submissions"
                            listItemIcon={
                                <ListItemIcon className={classes.listItemIcon}>
                                    {
                                        selectedMainMenuItem === ID_SUBMISSIONS ?
                                            <IdSubmissionsIcon className={classes.iconSelected} /> :
                                            <IdSubmissionsIcon className={classes.icon} />
                                    }
                                </ListItemIcon>
                            }
                            noti={true}
                            action={() => pushHistory(ID_SUBMISSIONS)} />
                        <MyListItem
                            selected={selectedMainMenuItem === USER_FEEDBACK}
                            classes={classes}
                            text="User Feedback"
                            listItemIcon={
                                <ListItemIcon className={classes.listItemIcon}>
                                    {
                                        selectedMainMenuItem === USER_FEEDBACK ?
                                            <UserFeedback className={classes.iconSelected} /> :
                                            <UserFeedback className={classes.icon} />
                                    }
                                </ListItemIcon>
                            }
                            noti={true}
                            action={() => pushHistory(USER_FEEDBACK)} />
                    </List>
                </Grid>
            </Grid>
        </>
    );

    useEffect(() => {
        if (location.pathname === '/') {
            setSelectedMainMenuItem(mainMenuPath)
        }
        if (selectedMainMenuItem !== mainMenuPath) {
            setSelectedMainMenuItem(mainMenuPath)
        }
    }, [location.pathname])

    return (
        <nav className={classes.drawer} aria-label="App navigations" id="noScrollBar">
            <Hidden smUp>
                <Drawer
                    container={container}
                    variant="temporary"
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={true}
                    // open={isDrawerMolibeOpen}
                    // onClose={handleMobileDrawerToggle}
                    classes={{
                        paper: classes.drawerPaper,
                    }}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                >
                    {drawer}
                    {/* <AppFooter /> */}
                </Drawer>
            </Hidden>
            <Hidden xsDown>
                <Drawer
                    classes={{
                        paper: classes.drawerPaper
                    }}
                    variant="persistent"
                    open={true}
                // open={isDrawerDesktopOpen}
                >
                    {drawer}
                </Drawer>
            </Hidden>
        </nav>
    )
});