import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/styles'
import AppLayout from './app-layout'
import theme from './theme'

function App() {
    return (
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <AppLayout />
            </ThemeProvider>
        </BrowserRouter>
    );
}

export default App;