import React from 'react'
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import clsx from 'clsx';
import { Grid } from '@material-ui/core'
import {
    ROOT,
    SIGNIN,
    SIGNUP,
    OVERVIEW,
    ID_SUBMISSIONS,
    USER_FEEDBACK
} from './paths';
import { makeStyles } from '@material-ui/styles';
import { Overview, SignIn, SignUp, IdSubmissions, UserFeedback } from './route-components';
import { LeftComponent } from './common';
import AppDrawer from './AppDrawer';
import { drawerWidth } from './context/LayoutContext';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        minHeight: '100vh',
        flexGrow: 1,
        padding: theme.spacing(2),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        [theme.breakpoints.down('md')]: {
            marginLeft: -drawerWidth,
        },
        [theme.breakpoints.up('md')]: {
            marginLeft: 0,
        },
    },
	contentShift: {
		transition: theme.transitions.create('margin', {
			easing: theme.transitions.easing.easeOut,
			duration: theme.transitions.duration.enteringScreen,
		}),
		marginLeft: 0,
	},
}))

const exclusivePaths = [SIGNIN, SIGNUP];

export default withRouter(function AppRoutes(props) {
    const classes = useStyles();
    return (
        <>
            <Route path={ROOT} render={({ location: { pathname } }) => {
                if (exclusivePaths.find(o => o === pathname)) {
                    return (
                        <Grid container>
                            <LeftComponent />
                            <Switch>
                                <Route path={SIGNIN} component={SignIn} />
                                <Route path={SIGNUP} component={SignUp} />
                            </Switch>
                        </Grid>
                    )
                }

                return (
                    <div className={classes.root}>
                        <AppDrawer />
                        <main className={clsx(classes.content, {
                            [classes.contentShift]: false,
                        })}>
                            <Switch>
                                <Route exact path={ROOT} component={Overview} />
                                <Route path={OVERVIEW} component={Overview} />
                                <Route path={ID_SUBMISSIONS} component={IdSubmissions} />
                                <Route path={USER_FEEDBACK} component={UserFeedback} />
                            </Switch>
                        </main>
                    </div>
                )
            }} />
        </>
    )
});
