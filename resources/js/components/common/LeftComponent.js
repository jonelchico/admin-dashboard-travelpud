import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Avatar } from '@material-ui/core';
import backgroundImage from '../assets/images/backgroundImage.png';
import logoIcon from '../assets/images/groupLogo.png';
import logoName from '../assets/images/travelpud-01.svg';

const useStyles = makeStyles(theme => ({
    leftContainer: {
        backgroundImage: `url(${backgroundImage})`,
        backgroundColor: theme.palette.primary.main,
        backgroundSize: 'cover',
        height: '100vh',
        paddingBottom: theme.spacing(2)
    },
    logo: {
        margin: 0,
        width: theme.spacing(5),
        height: theme.spacing(5),
    },
}))

const LeftComponent = () => {
    const classes = useStyles();
    const { leftContainer, logo } = classes;

    return (
        <Grid item container md={6} className={leftContainer} alignItems="center">
            <Grid container direction="column" alignItems="center" spacing={2}>
                <Grid item>
                    <Avatar alt="Travelpud" src={logoIcon} className={logo} />
                </Grid>
                <Grid item>
                    <img alt="Travelpud" src={logoName} />
                </Grid>
            </Grid>
        </Grid>
    )
};

export { LeftComponent };