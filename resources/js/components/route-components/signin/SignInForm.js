import React, { use } from 'react';
import { TextField, Grid, FormControl, Checkbox, FormGroup, FormControlLabel, Typography } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    formContainer: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        minWidth: 400
    },
    textField: {
        borderBottom: '1px solid #E9E9F0'
    },
    checkBoxPasswordContainer: {
        marginTop: theme.spacing(2),
    },
    forgotPasswordHover: {
        cursor: 'pointer'
    },
    buttonContainer: {
        marginTop: theme.spacing(2),
        cursor: 'pointer',
        backgroundColor: theme.palette.primary.main,
        height: theme.spacing(4),
        borderRadius: theme.spacing(2),
        color: 'white',
    }
}));

const CustomCheckBox = withStyles({
    root: {
        borderRadius: 12,
        color: '#808495',
        '&$checked': {
            color: '#808495',
        },
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const SignInForm = (props) => {
    const classes = useStyles();
    const { formContainer, textField, checkBoxPasswordContainer, forgotPasswordHover, buttonContainer } = classes
    const { values: { username, password, checkedBox }, handleChange, onSubmit, onKeyEnter} = props

    // console.log(values)
    return (
        <FormControl>
            <Grid container direction="column" spacing={1} className={formContainer}>
                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="username"
                        name="username"
                        fullWidth={true}
                        label="Username"
                        value={username}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>

                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="password"
                        type="password"
                        name="password"
                        fullWidth={true}
                        label="Password"
                        value={password}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>

                <Grid item xs={12} className={checkBoxPasswordContainer}>
                    <Grid container alignItems="center" justify="space-between">
                        <FormGroup>
                            <FormControlLabel
                                control={<CustomCheckBox value={checkedBox} size="small" onChange={handleChange} name="checkedBox" />}
                                label="Remember me"
                            />
                        </FormGroup>

                        <Typography className={forgotPasswordHover} onClick={() => console.log('forgot password')} >Forgot Password</Typography>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container className={buttonContainer} alignItems="center" justify="center"  onClick={() => onSubmit()} >
                <Typography variant="h5">Login</Typography>
            </Grid>
        </FormControl>
    )
};

export default SignInForm