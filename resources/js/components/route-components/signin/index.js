import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import SignInForm from './SignInForm';
import { ROOT, SIGNUP } from '../../paths'

const useStyles = makeStyles(theme => ({
    rightContainer: {
        backgroundColor: '#FFFFFF',
        height: '100vh',
        paddingBottom: theme.spacing(2)
    },
    header: {
        color: theme.palette.text.secondary,
        textAlign: 'center'
    },
    body2: {
        color: theme.palette.text.primaryOpacity50,
        textAlign: 'center'
    },
    createAccount: {
        marginTop: theme.spacing(3),
        textDecoration: 'underline',
        textAlign: 'center',
        cursor: 'pointer'
    }
}));

const SignIn = ({ history }) => {
    const classes = useStyles();
    const { rightContainer, logo, header, body2, createAccount } = classes;

    const [values, setValues] = useState({
        username: '',
        password: '',
        checkedBox: false,
    });

    const handleChange = (event) => {
        if (event.target.name === 'checkedBox') {
            setValues({ ...values, [event.target.name]: event.target.checked })
        } else {
            setValues({ ...values, [event.target.name]: event.target.value })
        }
    };

    const onSubmit = () => {
        history.push(ROOT)
    }

    const onKeyEnter = (event) => {    
        if (event.keyCode && event.keyCode === 13) {
            onSubmit()
        }
    }

    return (
        <Grid item container md={6} className={rightContainer} alignItems="center">
            <Grid container direction='column' alignContent="center" >
                <Grid item>
                    <Typography className={header} variant="h6">Sign in</Typography>
                    <Typography className={body2} variant="body2">Welcome back! Please login to your account.</Typography>
                </Grid>
                <Grid item>
                    <SignInForm
                        values={values}
                        handleChange={(event) => handleChange(event)}
                        onSubmit={onSubmit}
                        onKeyEnter={onKeyEnter}
                    />
                </Grid>
                <Grid item>
                    <Typography className={createAccount} onClick={() => history.push(SIGNUP)}>Not Registered? Create an Account!</Typography>
                </Grid>
            </Grid>
        </Grid>
    )
};

export { SignIn }