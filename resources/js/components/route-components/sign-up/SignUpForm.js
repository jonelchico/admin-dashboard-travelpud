import React, { use } from 'react';
import { TextField, Grid, FormControl, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
    formContainer: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        minWidth: 400
    },
    textField: {
        borderBottom: '1px solid #E9E9F0'
    },
    textFieldFirstName: {
        borderBottom: '1px solid #E9E9F0',
    },
    buttonContainer: {
        marginTop: theme.spacing(2),
        cursor: 'pointer',
        backgroundColor: theme.palette.primary.main,
        height: theme.spacing(4),
        borderRadius: theme.spacing(2),
        color: 'white',
    }
}));

const SignUpForm = (props) => {
    const classes = useStyles();
    const { formContainer, textField, textFieldFirstName, buttonContainer } = classes
    const { values: { firstName, lastName, email, mobileNumber, password, confirmPassword }, handleChange, onSubmit, onKeyEnter } = props

    return (
        <FormControl>
            <Grid container direction="column" spacing={1} className={formContainer}>
                <Grid item container xs={12} justify="space-between">
                    <Grid item xs={false} className={textFieldFirstName}>
                        <TextField
                            disabled={false}
                            id="firstName"
                            name="firstName"
                            fullWidth={true}
                            label="First Name"
                            value={firstName}
                            onChange={handleChange}
                            InputProps={{
                                disableUnderline: true
                            }}
                            onKeyDown={onKeyEnter}
                        />
                    </Grid>

                    <Grid item xs={false} className={textField}>
                        <TextField
                            disabled={false}
                            id="lastName"
                            name="lastName"
                            fullWidth={true}
                            label="Last Name"
                            value={lastName}
                            onChange={handleChange}
                            InputProps={{
                                disableUnderline: true
                            }}
                            onKeyDown={onKeyEnter}
                        />
                    </Grid>
                </Grid>

                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="email"
                        name="email"
                        fullWidth={true}
                        label="Email"
                        value={email}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>

                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="mobileNumber"
                        name="mobileNumber"
                        fullWidth={true}
                        label="Mobile Number"
                        value={mobileNumber}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>

                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="password"
                        type="password"
                        name="password"
                        fullWidth={true}
                        label="Password"
                        value={password}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>

                <Grid item xs={12} className={textField}>
                    <TextField
                        disabled={false}
                        id="confirmPassword"
                        type="password"
                        name="confirmPassword"
                        fullWidth={true}
                        label="Confirm Password"
                        value={confirmPassword}
                        onChange={handleChange}
                        InputProps={{
                            disableUnderline: true
                        }}
                        onKeyDown={onKeyEnter}
                    />
                </Grid>
            </Grid>
            <Grid container className={buttonContainer} alignItems="center" justify="center" onClick={() => onSubmit()} >
                <Typography variant="h5">Sign up</Typography>
            </Grid>
        </FormControl>
    )
};

export default SignUpForm