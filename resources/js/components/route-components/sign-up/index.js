import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import SignUpForm from './SignUpForm'
import { SIGNIN } from '../../paths'

const useStyles = makeStyles(theme => ({
    rightContainer: {
        backgroundColor: '#FFFFFF',
        height: '100vh',
        paddingBottom: theme.spacing(2)
    },
    header: {
        color: theme.palette.text.secondary,
        textAlign: 'center'
    },
    body2: {
        color: theme.palette.text.primaryOpacity50,
        textAlign: 'center'
    },
    createAccount: {
        marginTop: theme.spacing(3),
        textDecoration: 'underline',
        textAlign: 'center',
        cursor: 'pointer'
    }
}));

const SignUp = ({ history }) => {
    const classes = useStyles();
    const { rightContainer, logo, header, body2, createAccount } = classes

    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        email: '',
        mobileNumber: '',
        password: '',
        confirmPassword: '',
    });

    const handleChange = (event) => {
        setValues({ ...values, [event.target.name]: event.target.value })
    };

    const onSubmit = () => {
        history.push(SIGNIN)
    }

    const onKeyEnter = (event) => {
        if (event.keyCode && event.keyCode === 13) {
            onSubmit()
        }
    }

    return (
        <Grid item container md={6} className={rightContainer} alignItems="center">
            <Grid container direction='column' alignContent="center" >
                <Grid item>
                    <Typography className={header} variant="h6">Sign up</Typography>
                    <Typography className={body2} variant="body2">Create your account by filling in the form below.</Typography>
                </Grid>
                <Grid item>
                    <SignUpForm
                        values={values}
                        handleChange={(event) => handleChange(event)}
                        onSubmit={onSubmit}
                        onKeyEnter={onKeyEnter}
                    />
                </Grid>
                <Grid item>
                    <Typography className={createAccount} onClick={() => history.push(SIGNIN)}>Already have an account? Sign in.</Typography>
                </Grid>
            </Grid>
        </Grid>
    )
};

export { SignUp }