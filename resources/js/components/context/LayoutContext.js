import React from 'react';

export const drawerWidth = 293;

export const LayoutContext = React.createContext({
	isDrawerMolibeOpen: false,
	isDrawerDesktopOpen: true,
	handleDesktopDrawerToggle: () => {},
	handleMobileDrawerToggle: () => {}
});